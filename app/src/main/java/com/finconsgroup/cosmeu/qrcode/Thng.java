package com.finconsgroup.cosmeu.qrcode;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;


public class Thng {
        private String pik;
        private String id; //EVRYTHNG ID
        private String name;
        private String idk64;
        private String product_name;
        private String authentic;
        private byte[] idk;
        private HashMap<String, String> customFields;
        private HashMap<String,String> toShow;

        private HashMap<String, HashMap<String, String>> product_info;

        private final String[] production_info = {"BatchDate", "BatchID"};
        private final String[] delivery_info = {"BatchDistributor", "BatchMarket"};


        /**
         * Parses the JSON data returned by EVRYTHNG
         * @param blob
         */
        public Thng(JSONObject blob){
            try {
                Gson gson = new Gson();
                id = blob.getString("id");
                name = blob.getString("name");
                JSONObject identifiers = blob.getJSONObject("identifiers");
                pik = identifiers.getString("pik");
                JSONObject cf = blob.getJSONObject("customFields");
                Type type = new TypeToken<HashMap<String, String>>(){}.getType();
                //TODO: filter out non-customer releted fields
                customFields = gson.fromJson(cf.toString(), type);
                product_name = customFields.get("ProductName");
                authentic = customFields.get("Authentic");
                product_info = new HashMap<>();
                product_info.put("Production info", new HashMap<String, String>());
                product_info.put("Delivery info", new HashMap<String, String>());
                toShow = new HashMap<String, String>();
                Arrays.sort(production_info);
                Arrays.sort(delivery_info);

                fieldsToShow();



                idk64 = customFields.get("idk");
                Log.i("idk64",idk64);




            } catch (JSONException e) {
                Log.e("Virtual Entity","Failed to parse virtual entity data");
                e.printStackTrace();
            }
        }

        public String getPik() {
            return pik;
        }

        public String getId() { return id;  }



        public String getName() {
            return name;
        }

        public String getProductName(){
            return product_name;
        }

        public Boolean isAuthentic(){
         return (authentic.compareTo("true")>-1)? true:false;
        }

        public String getIdk64() {
            return idk64;
        }

        public byte[] getIdk() {return idk;}

        public HashMap<String, String> getFieldsToShow() {
            return toShow;
        }

        public HashMap<String, String> getProductInfo(String key) {
            return product_info.get(key);
        }



        private void fieldsToShow(){
            toShow.put("Production info", "");
            toShow.put("Delivery info", "");
            for (String s : customFields.keySet()){
                //Add base root url to relative path. customField.get("base_url") + customFields.get(s)
                //if(s.endsWith("_cpabe"))toShow.put(s.replaceFirst("_cpabe",""),customFields.get(s)); //get all files
                Log.d("Keyset from customFiled", s);
            /*
              Checkz if the key of the custom field is part of the buttons group, or contains the placeholder "#FILE#".
              If true, it puts the String key of the field in the array of elements to show.
             */
                if(s.startsWith("#FILE#")){
                    toShow.put(s.replace("#FILE#", ""),customFields.get(s));
                } else if((Arrays.binarySearch(production_info, s) >= 0)){
                    product_info.get("Production info").put(s, customFields.get(s));
                } else if((Arrays.binarySearch(delivery_info, s) >= 0)){
                    product_info.get("Delivery info").put(s, customFields.get(s));
                }
                //if(s.startsWith("?") || s.equals("BatchDate") || s.equals("BatchID") || s.equals("BatchDistributor") || s.equals("BatchMarket")) toShow.put(s.replaceFirst("v-", ""),customFields.get(s));
            }

        }

        public String getField(String filename){
            return customFields.get(filename);
        }

        public String getFieldValueToShow(String filename){
            return toShow.get(filename);
        }


        public String getFileHash(String filename){
            Log.d("TNG filename", filename);
            return customFields.get(filename+"_hash");
        }
}
