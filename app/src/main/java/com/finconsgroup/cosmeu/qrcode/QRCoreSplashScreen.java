package com.finconsgroup.cosmeu.qrcode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.finconsgroup.cosmeu.R;

import org.json.JSONException;
import org.json.JSONObject;

public class QRCoreSplashScreen extends AppCompatActivity {
    private JSONObject full_json_info;
    private ImageView imageView;
    private TextView title;
    private TextView status;
    private TextView description;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_splash);
        imageView= findViewById(R.id.imageView1);
        title=findViewById(R.id.title);
        status=findViewById(R.id.status);
        description = findViewById(R.id.description);
        button=findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QRCoreSplashScreen.this,QRCodeRenderActivity.class);
                Bundle extras = new Bundle();
                extras.putString("full_json_info", full_json_info.toString());
                intent.putExtras(extras);
                startActivity(intent);
                finish();
            }
        });

        try {
            full_json_info= new JSONObject(getIntent().getExtras().getString("full_json_info"));
            updateUI(full_json_info);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void updateUI (JSONObject result) throws JSONException {
        JSONObject json_file = result.getJSONObject("File");
        title.setText(json_file.get("Product_name").toString()+" "+json_file.get("Brand_name").toString());
        title.setText(json_file.get("Product_name").toString()+" "+json_file.get("Brand_name").toString());
        title.setText(json_file.get("Product_name").toString()+" "+json_file.get("Brand_name").toString());
        if(result.getBoolean("authentic")){
            imageView.setImageResource(R.drawable.checked);
            status.setText(R.string.authenticText);
            description.setText(R.string.authenticDescription);
        }else{
            imageView.setImageResource(R.drawable.unchecked);
            status.setText(R.string.notAuthenticText);
            description.setText(R.string.NotAuthenticDescription);
        }
    }


}

