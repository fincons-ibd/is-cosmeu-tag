package com.finconsgroup.cosmeu.qrcode;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.finconsgroup.cosmeu.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class HorizontalNotesAdapter extends RecyclerView.Adapter<HorizontalNotesAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private JSONArray notes;
    private String base_url;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView riv;
        private TextView image_name;

        public MyViewHolder(View view) {
            super(view);
            image_name = view.findViewById(R.id.image_name);
            riv = (ImageView) view.findViewById(R.id.horizontal_item_view_image);

        }
    }


    public HorizontalNotesAdapter(Context context, JSONArray notes) {
        this.context = context;
        this.notes = notes;
        base_url = context.getResources().getString(R.string.conf_notes_images_base_url);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.qrcode_item_recycler_notes, parent, false);

        if (itemView.getLayoutParams ().width == RecyclerView.LayoutParams.MATCH_PARENT)
            itemView.getLayoutParams ().width = RecyclerView.LayoutParams.WRAP_CONTENT;

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            Glide.with(this.context)
                    .load(base_url+notes.getJSONObject(position).getString("image_name"))
                    .apply(new RequestOptions().error(R.drawable.not_found))
                    .into(holder.riv);
            holder.image_name.setText(notes.getJSONObject(position).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return notes.length();
    }
}