package com.finconsgroup.cosmeu.qrcode;

import org.json.JSONObject;


public interface VolleyCallback {
    void onSuccess(JSONObject result);
    void onFail();
}
