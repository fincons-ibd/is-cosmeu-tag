package com.finconsgroup.cosmeu.qrcode;


import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.finconsgroup.cosmeu.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class EvrythngAPI {
    private static EvrythngAPI mInstance;
    private RequestQueue mRequestQueue;

    private static Context mCtx;

    private final String root;
    private final String APIKEY;
    private String userkey;


    private EvrythngAPI(Context context) {
        userkey = null;
        mCtx = context;
        root = context.getString(R.string.evrythng_api_endpoint);
        APIKEY = context.getString(R.string.evrythng_api);
        //ANDROID SSL FIX FOR <=KITKAT
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            HttpStack stack = null;
            try {
                stack = new HurlStack(null, new TLSSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
                Log.d("EVRYTHNGAPI", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                Log.d("EVRYTHNGAPI", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            }
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext(), stack);
        } else {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }




    }

    /**
     * Returns the singleton instance of the ApiManager
     * Uses the APIKEY specified in strings.xml
     * @param context
     * @return EvrythngAPI instance
     */


    public static synchronized EvrythngAPI getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new EvrythngAPI(context);
        }
        return mInstance;
    }



    public <T> void addToRequestQueue(Request<T> req) {
        mRequestQueue.add(req);
    }


    private void authAnonUser(final VolleyCallback callback) {
        String url = root + "/auth/evrythng/users?anonymous=true";
        JSONObject reqBody = null;
        try {
            reqBody = new JSONObject("{\"anonymous\": true}");
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url,reqBody,new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        userkey = response.getString("evrythngApiKey");
                        callback.onSuccess(null);
                        Log.i("EVRYTHNGAPI_login",userkey);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callback.onFail();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("EVRYTHNGAPI_login","anon user authentication error: " + error);
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", APIKEY);
                    return params;
                }
            };
            mRequestQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the THNG with the specified pik identifier
     * GET /thngs?filter=identifiers.pik={pik}
     * @param pik
     * @param callback
     */


    public void getThng(String pik, final VolleyCallback callback)  {
        String url = root +"/thngs?filter=identifiers.pik="+pik;
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,url,null,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.i("EVRYTHNGAPI_getThng",response.toString());
                if(response.length() != 0) {
                    try {
                        JSONObject result = response.getJSONObject(0);//ONLY ONE RESULT
                        callback.onSuccess(result);
                    } catch (JSONException e) {
                        Log.e("EVRYTHNGAPI_getThng", "JSON Response error");
                        e.printStackTrace();
                    }
                }else callback.onFail();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EVRYTHNGAPI_getThng",error.getMessage());

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", userkey);
                return params;
            }
        };
        //If user isn't logged in yet, log in and then retrieve the thng
        if(userkey == null){
            authAnonUser(new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject result) {
                    mRequestQueue.add(request); //Add getThng request to volley queue
                }
                @Override
                public void onFail(){
                    Log.e("EVRYTHNG","Not authorized");
                    Toast.makeText(mCtx, "ERROR: Not Authorized", Toast.LENGTH_LONG).show();

                }
            });
        }else{
            mRequestQueue.add(request);

        }

    }



}
