/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.finconsgroup.cosmeu.ar.augmentedimage;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.finconsgroup.cosmeu.R;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Pose;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CompletableFuture;

public class AugmentedImageNode extends Node implements Node.OnTapListener {

    private static final String TAG = "AugmentedImageNode";

    private AugmentedImage image;
    private CompletableFuture<ModelRenderable> model;

    private Bundle bundle ;
    private Node infoCard;
    private Context context;
    JSONObject json_file;

    public AugmentedImageNode(Context context, String model_path, JSONObject jsonPerfume) {
        this.context=context;
        // Upon construction, start loading the models for the corners of the frame.
        if(model == null){
            try {
                json_file= jsonPerfume.getJSONObject("File");
                model = ModelRenderable.builder()
                        .setSource(context,Uri.parse(model_path))
                        .build();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        this.bundle=bundle;
        setOnTapListener(this);




    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image.  The corners are then positioned based
     * on the extents of the image.  There is no need to worry about world coordinates since everything
     * is relative to the center of the image, which is the parent node of the corners.
     *
     * @param image
     */
    public void setImage(AugmentedImage image) {
        this.image = image;

        if (!model.isDone()) {
            CompletableFuture.allOf(model).thenAccept((Void aVoid) -> {
                setImage(image);
            })
                    .exceptionally(throwable -> {
                        Log.e(TAG, "Exception loading", throwable);
                        return null;
                    });
        }

        // Make the 4 corner nodes.
        Pose pose= image.getCenterPose();
        Vector3 localPosition = new Vector3(pose.tx(),pose.getYAxis()[0]+0.02f,pose.tz());
        Quaternion localRotation = new Quaternion();
        Node node;

        // Center image
        //localPosition.set(0.0f * image.getExtentX(), 0.0f, 1.0f * image.getExtentZ());
        //localPosition.set(0.0f * image.getExtentX(), 0.0f, image.getExtentZ()-0.1f);
        // Rotate model
        //localRotation.set(0.7071f , 0f, 0f,0.7071f);

        node = new Node();
        node.setParent(this);
        node.setLocalPosition(localPosition);
        node.setLocalRotation(localRotation);
        node.setRenderable(model.getNow(null));

        if(infoCard==null){
            infoCard = new Node();
            infoCard.setParent(this);
            infoCard.setEnabled(true);
            //localPosition.set(0.0f * image.getExtentX(), 0.0f, 1.0f * image.getExtentZ());
            Vector3 infoCardLocalPosition = localPosition;
            infoCardLocalPosition.y+=0.23f;
            infoCard.setLocalPosition(infoCardLocalPosition);
            infoCard.setLocalRotation(new Quaternion(0.7071f,0f,0f,-0.7071f));

            ViewRenderable.builder()
                    .setView(context, R.layout.perfume_model_text)
                    .build()
                    .thenAccept(
                            (renderable) -> {
                                infoCard.setRenderable(renderable);
                                View infocardView = renderable.getView();
                                TextView Note_di_testa_value = infocardView.findViewById(R.id.Note_di_testa_value);
                                TextView Note_di_cuore_value = infocardView.findViewById(R.id.Note_di_cuore_value);
                                TextView Note_di_fondo_value = infocardView.findViewById(R.id.Note_di_fondo_value);
                                try {
                                    Note_di_testa_value.setText(JSONArrayToStringComma(json_file.getJSONArray("Heart_notes")));
                                    Note_di_cuore_value.setText(JSONArrayToStringComma(json_file.getJSONArray("Head_notes")));
                                    Note_di_fondo_value.setText(JSONArrayToStringComma(json_file.getJSONArray("Base_notes")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            })
                    .exceptionally(
                            (throwable) -> {
                                throw new AssertionError("Could not load plane card view.", throwable);
                            });
        }


    }

    public AugmentedImage getImage() {
        return image;
    }

    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void onActivate() {
        if (getScene() == null) {
            throw new IllegalStateException("Scene is null!");
        }
    }

    @Override
    public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {
        if (infoCard == null) {
            return;
        }

        infoCard.setEnabled(!infoCard.isEnabled());
    }

    @Override
    public void onUpdate(FrameTime frameTime) {
        if (infoCard == null) {
            return;
        }

        // Typically, getScene() will never return null because onUpdate() is only called when the node
        // is in the scene.
        // However, if onUpdate is called explicitly or if the node is removed from the scene on a
        // different thread during onUpdate, then getScene may be null.
        if (getScene() == null) {
            return;
        }
        Vector3 cameraPosition = getScene().getCamera().getWorldPosition();
        Vector3 cardPosition = infoCard.getWorldPosition();
        Vector3 direction = Vector3.subtract(cameraPosition, cardPosition);
        Quaternion lookRotation = Quaternion.lookRotation(direction, Vector3.up());
        infoCard.setWorldRotation(lookRotation);
    }


    public static String JSONArrayToStringComma(JSONArray arrayToConvert) throws JSONException {
        String stringsComma = "";
        int i;
        for (i=0; i < arrayToConvert.length()-1; i++) {
            stringsComma=stringsComma.concat(arrayToConvert.getJSONObject(i).getString("name") + ", ");
        }
        stringsComma=stringsComma.concat(arrayToConvert.getJSONObject(i).getString("name") + ".");
        return stringsComma;
    }
}
