/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.finconsgroup.cosmeu.ar.augmentedimage;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.finconsgroup.cosmeu.R;
import com.finconsgroup.cosmeu.ar.common.helpers.SnackbarHelper;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Session;
import com.google.ar.sceneform.ux.ArFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import static com.finconsgroup.cosmeu.ar.augmentedimage.AugmentedImageActivity.fitToScanView;

/**
 * Extend the ArFragment to customize the ARCore session configuration to include Augmented Images.
 */
public class AugmentedImageFragment extends ArFragment {
  private static final String TAG = "AugmentedImageFragment";
  private String base_url ;
  // This is the name of the image in the sample database.  A copy of the image is in the assets
  // directory.  Opening this image on your computer is a good quick way to test the augmented image
  // matching.

  // Do a runtime check for the OpenGL level available at runtime to avoid Sceneform crashing the
  // application.
  private static final double MIN_OPENGL_VERSION = 3.1;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    // Check for Sceneform being supported on this device.  This check will be integrated into
    // Sceneform eventually.
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
      Log.e(TAG, "Sceneform requires Android N or later");
      SnackbarHelper.getInstance()
              .showError(getActivity(), "Sceneform requires Android N or later");
    }

    String openGlVersionString =
            ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
                    .getDeviceConfigurationInfo()
                    .getGlEsVersion();
    if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
      Log.e(TAG, "Sceneform requires OpenGL ES 3.1 or later");
      SnackbarHelper.getInstance()
              .showError(getActivity(), "Sceneform requires OpenGL ES 3.1 or later");
    }
  }

  @Override
  public View onCreateView(
          LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);

    // Turn off the plane discovery since we're only looking for images
    getPlaneDiscoveryController().hide();
    getPlaneDiscoveryController().setInstructionView(null);
    getArSceneView().getPlaneRenderer().setEnabled(false);
    base_url = getResources().getString(R.string.conf_base_url);
    return view;
  }

  @Override
  protected Config getSessionConfiguration(Session session) {
    Config config = super.getSessionConfiguration(session);
    new BackgroundTask(config, session,getContext()).execute();
    return config;
  }

  private class BackgroundTask extends AsyncTask<Void, Void, Void> {
    Config config;
    Session session;
    Context context;
    ProgressDialog progressDialog;

    BackgroundTask(Config config, Session session,Context context) {
      this.config = config;
      this.session = session;
      this.context = context;
    }
    protected void onPreExecute() {
      progressDialog = new ProgressDialog(context,R.style.AppThemeMyLight);
      progressDialog.setMessage(getString(R.string.loading_ar));
      progressDialog.setCancelable(false);
      progressDialog.show();
    }

    protected Void doInBackground(Void... urls) {
      try {
        AugmentedImageDatabase augmentedImageDatabase = null;
        Iterator<String> keys = AugmentedImageActivity.json_Box_images.keys();
        augmentedImageDatabase = new AugmentedImageDatabase(session);
        while (keys.hasNext()) {
          String key = keys.next();
          String value = AugmentedImageActivity.json_Box_images.getString(key);
          String urldisplay = base_url+value;
          try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            Bitmap augmentedImageBitmap = BitmapFactory.decodeStream(in);
            if (augmentedImageBitmap != null) {
              augmentedImageDatabase.addImage(key, augmentedImageBitmap);
            }
          } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
          }

        }
        config.setAugmentedImageDatabase(augmentedImageDatabase);
        session.configure(config);
      } catch (JSONException e) {
        e.printStackTrace();

      }
      return null;
    }

    protected void onPostExecute(Void result) {
      progressDialog.dismiss();
      fitToScanView.setVisibility(View.VISIBLE);
      SnackbarHelper.getInstance()
              .showMessage(getActivity(), getResources().getString(R.string.ar_setted_successfully));
    }
  }

}
