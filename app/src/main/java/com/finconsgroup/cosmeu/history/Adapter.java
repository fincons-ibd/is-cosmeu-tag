package com.finconsgroup.cosmeu.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finconsgroup.cosmeu.R;
import com.finconsgroup.cosmeu.qrcode.QRCodeActivity;
import com.finconsgroup.cosmeu.qrcode.QRCodeRenderActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
    private Context context;
    private List<String> mItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Product_name, Brand_name, Timestamp;
        public ImageView thumbnail;
        public RelativeLayout viewBackground, viewForeground;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, QRCodeRenderActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("full_json_info", mItems.get(getAdapterPosition()));
                    intent.putExtras(extras);
                    context.startActivity(intent);
                }
            });
            Product_name = view.findViewById(R.id.Product_name);
            Brand_name = view.findViewById(R.id.Brand_name);
            Timestamp = view.findViewById(R.id.Timestamp);
            thumbnail = view.findViewById(R.id.imageView);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }


    public Adapter(Context context, List<String> mItems) {
        this.context = context;
        this.mItems = mItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        JSONObject temp = null;
        try {
            temp = new JSONObject(mItems.get(position));
            JSONObject File = temp.getJSONObject("File");

            holder.Product_name.setText(File.getString("Product_name"));
            holder.Brand_name.setText(File.getString("Brand_name"));
            if(temp.getBoolean("authentic")){
                holder.thumbnail.setImageResource(R.drawable.checked);
            }else{
                holder.thumbnail.setImageResource(R.drawable.unchecked);
            }
            Date scanned_day = new Date(temp.getString("Timestamp"));
            Date today_date = new Date();
            SimpleDateFormat dateFormat;
            if (isBeforeToday(scanned_day,today_date)){
                dateFormat= new SimpleDateFormat("H:mm");
            }else{
                dateFormat= new SimpleDateFormat("dd/MM/yyyy");
            }
            holder.Timestamp.setText(dateFormat.format(scanned_day));

            //Glide.with(recyclerViewHolder.home_image.getContext()).load(base_url+Multimedia_assets.getString("Home_image")).into(recyclerViewHolder.home_image); //>>not tested


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void removeItem(int position) {
        mItems.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(String item, int position) {
        mItems.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public static boolean isBeforeToday(Date scanned_day, Date today) {
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        boolean result = today.getTime() < scanned_day.getTime();
        return result;
    }

}