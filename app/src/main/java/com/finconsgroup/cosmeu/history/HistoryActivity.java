package com.finconsgroup.cosmeu.history;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import com.finconsgroup.cosmeu.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.finconsgroup.cosmeu.qrcode.QRCodeRenderActivity.manipulateColor;

public class HistoryActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private CoordinatorLayout coordinatorLayout;

    private Adapter adapter;
    private int color = 0;
    private List<String> data;
    private String insertData;
    private boolean loading;
    private int loadTimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = findViewById(R.id.toolbar_recycler_view);
        toolbar.setTitle(R.string.history);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        load_data();
        initView();
    }

    private void load_data() {
        data = new ArrayList<>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        try {
            JSONArray saved_date = new JSONArray( pref.getString("full_json_info", "[]"));
            for (int i=saved_date.length()-1; i >= 0; i--) {
                JSONObject temp = saved_date.getJSONObject(i);
                data.add(temp.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void initView() {

        ImageView image_scrolling_top = findViewById(R.id.image_scrolling_top);
        CollapsingToolbarLayout collapsing_toolbar_layout= findViewById(R.id.collapsing_toolbar_layout);
        int vibrant = Color.parseColor("#93673C");
        collapsing_toolbar_layout.setBackgroundColor(vibrant);
        collapsing_toolbar_layout.setStatusBarScrimColor(vibrant);
        collapsing_toolbar_layout.setContentScrimColor(vibrant);
        //getWindow().setNavigationBarColor(muted_dark);
        getWindow().setStatusBarColor(manipulateColor(vibrant,0.8f));
        mRecyclerView = findViewById(R.id.recycler_view);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        adapter = new Adapter(this, data);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(adapter);

        // adding item touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);


    }
    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof Adapter.MyViewHolder) {

            String deletedItem = data.get(position);
            adapter.removeItem(viewHolder.getAdapterPosition());
            //REMOVE ELEMENT FROM HISTORY DB
            SharedPreferences pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            try {
                JSONArray saved_date = new JSONArray( pref.getString("full_json_info", "[]"));
                saved_date.remove(position);
                editor.putString("full_json_info", saved_date.toString());
                editor.commit();
                if (saved_date.length()==0){
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.perfume_removed), Snackbar.LENGTH_LONG);
            snackbar.setAction(getResources().getString(R.string.item_swipe_undo), new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    adapter.restoreItem(deletedItem, position);
                    //RESTORE ELEMENT FROM HISTORY DB
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    try {
                        JSONArray saved_date = new JSONArray( pref.getString("full_json_info", "[]"));
                        saved_date.put(position,new JSONObject(deletedItem));
                        editor.putString("full_json_info", saved_date.toString());
                        editor.commit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    
    private int getScreenWidthDp() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return (int) (displayMetrics.widthPixels / displayMetrics.density);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}