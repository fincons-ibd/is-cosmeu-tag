package com.finconsgroup.cosmeu.intro;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;

import com.finconsgroup.cosmeu.R;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;
import com.heinrichreimersoftware.materialintro.slide.Slide;

public class MainIntroActivity extends IntroActivity {
    @Override protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setButtonBackVisible(false);
        setButtonNextVisible(false);
        setButtonCtaVisible(true);
        setButtonCtaTintMode(BUTTON_CTA_TINT_MODE_BACKGROUND);
        TypefaceSpan labelSpan = new TypefaceSpan(
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? "sans-serif-medium" : "sans serif");
        SpannableString label = SpannableString
                .valueOf(getString(R.string.label_button_cta_canteen_intro));
        label.setSpan(labelSpan, 0, label.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        setButtonCtaLabel(label);

        setPageScrollDuration(500);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setPageScrollInterpolator(android.R.interpolator.fast_out_slow_in);
        }

        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro1_title)
                .description(R.string.intro1_description)
                .image(R.drawable.intro1)
                .background(R.color.intro1)
                .backgroundDark(R.color.intro1_dark)
                .layout(R.layout.slide_canteen)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro2_title)
                .description(R.string.intro2_description)
                .image(R.drawable.intro2)
                .background(R.color.intro2)
                .backgroundDark(R.color.intro2_dark)
                .layout(R.layout.slide_canteen)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro3_title)
                .description(R.string.intro3_description)
                .image(R.drawable.intro3)
                .background(R.color.intro3)
                .backgroundDark(R.color.intro3_dark)
                .layout(R.layout.slide_canteen)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro4_title)
                .description(R.string.intro4_description)
                .image(R.drawable.intro4)
                .background(R.color.intro4)
                .backgroundDark(R.color.intro4_dark)
                .layout(R.layout.slide_canteen)
                .build());

        if (!checkCameraPermission()) {

            addSlide(new SimpleSlide.Builder()
                    .title(R.string.intro5_title)
                    .description(R.string.intro5_description)
                    .image(R.drawable.intro5)
                    .background(R.color.intro5)
                    .backgroundDark(R.color.intro5_dark)
                    .layout(R.layout.slide_canteen)
                    .permissions(new String[]{Manifest.permission.CAMERA})
                    .build());
        }
    }


    private boolean checkCameraPermission()
    {
        String permission = Manifest.permission.CAMERA;
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
