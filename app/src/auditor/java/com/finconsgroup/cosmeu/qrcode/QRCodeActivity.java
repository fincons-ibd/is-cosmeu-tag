package com.finconsgroup.cosmeu.qrcode;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.finconsgroup.cosmeu.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.bouncycastle.util.encoders.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cpabe.AttributeException;
import cpabe.Common;
import cpabe.Cpabe;


public class QRCodeActivity extends AppCompatActivity {
    //private String base_url = "http://10.0.2.2:8082/";
    //private String base_url = "http://172.25.15.242:8082/"; //TODO: add base_url to strings.xml
    //private String base_url = "http://172.25.12.108:8082/";
    private String base_url;
    private EvrythngAPI api;
    private Thng t;
    private ImageView perfume_image;
    private Toolbar toolbar;
    ProgressDialog progressDialog;
    private Cpabe cpabe;
    private String idk;
    private String thng_value;
    private HashMap<String, String> product_info;
    private String filehash;
    String prv_path;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_scanning);
        base_url = getResources().getString(R.string.conf_base_url);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        prv_path = pref.getString("prv_path","ERROR");
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        if (check_internet()) {
            api = EvrythngAPI.getInstance(this);
            init_scan();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Info");
            alertDialog.setMessage("Internet not available, check your internet connectivity and try again");
            alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            alertDialog.show();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get thng from scanned pik
        progressDialog = new ProgressDialog(QRCodeActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                finish();
                //getThng("3ae123b130bdb0cdfa72cc4a03d5e594c4330d0b "); //TODO: delete me
            } else {
                getThng(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getThng(final String pik) {
        api.getThng(pik, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                Log.i("Thng", result.toString());
                t = new Thng(result);
                new DownloadFilesTask(t).execute();
            }

            @Override
            public void onFail() {
                Toast.makeText(getApplicationContext(), "Please scan a valid perfume QRCODE", Toast.LENGTH_LONG).show();
                init_scan();
            }

        });
    }

    private void init_scan() {
        //initiate scan with zxing capture activity
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan QRCODE");
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    private boolean check_internet() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    private byte[] decrypt(String enc_file_path) throws AttributeException, Exception {
        cpabe = new Cpabe();
        Log.d("cpabe","start decryption");
        byte[] pub = Base64.decode(getString(R.string.pubkey), Base64.DEFAULT);
        byte[] prv = Base64.decode(idk, Base64.URL_SAFE);
        byte[] output = null;
        output = cpabe.dec(pub,prv, Common.readCpabeFile(enc_file_path));
        return output;
    }

    private class DownloadFilesTask extends AsyncTask<Void, Void, Void> {

        private Thng t;
        // CPABE
        private JSONObject full_json_info = new JSONObject();


        public DownloadFilesTask(Thng t){
            this.t=t;
            try {
                full_json_info.put("Brand_name",t.getProductName().toString());
                full_json_info.put("Timestamp",new Date());
                full_json_info.put("authentic",t.isAuthentic());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        protected Void  doInBackground(Void... params) {
            // code that will run in the background

            for(Map.Entry<String, String> entry : t.getFieldsToShow().entrySet()) {
                String key = entry.getKey();
                product_info = t.getProductInfo(key);
                idk = prv_path;
                thng_value=t.getFieldValueToShow(key);
                filehash = t.getFileHash(key);
                switch (key) {
                    case ("Production info"):
                    case ("Delivery info"):
                        try {
                            full_json_info.put(key.replace(" ","_"),getProductInfo(product_info, base_url, key));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        try {
                            full_json_info.put("File",new JSONObject(DownloadFileFromURLandDec(filehash, base_url + thng_value.replace("#FILE#", "").replace("*","."))));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
            return null;
        }


        protected void onPostExecute(Void params) {
            // update the UI after background processes completes
            progressDialog.dismiss();
            //SAVE HISTORY
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            try {
                JSONArray saved_date = new JSONArray( pref.getString("full_json_info", "[]"));
                saved_date.put(full_json_info);
                editor.putString("full_json_info", saved_date.toString());
                editor.commit();
                Intent intent = new Intent(QRCodeActivity.this, QRCoreSplashScreen.class);
                Bundle extras = new Bundle();
                extras.putString("full_json_info", full_json_info.toString());
                intent.putExtras(extras);
                startActivity(intent);
                Log.i("************** full_json_info: ",full_json_info.toString());
                //VALIDATE JSON
                JSONObject json_file = full_json_info.getJSONObject("File");
                json_file.get("Product_name");
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Please scan a valid QRcode", Toast.LENGTH_LONG).show();
                finish();
            }
            finish();
        }
    }


    private JSONObject getProductInfo(HashMap<String, String> product_info, String base_url, String itemTypeClicked) throws JSONException {

        JSONObject jsonResult=new JSONObject();
        byte[][] input_enc = null ;
        byte[] output = null;
        String value="";
        Set<String> keys = product_info.keySet();
        for(String key : keys) {
            input_enc = new byte[2][];
            String[] inputb64 = product_info.get(key).split(" ");
            input_enc[0] = Base64.decode(inputb64[0], Base64.URL_SAFE);
            input_enc[1] = Base64.decode(inputb64[1], Base64.URL_SAFE);
            byte[][] encrypted_input = input_enc;
            Cpabe cpabe = new Cpabe();
            byte[] pub = Base64.decode(getString(R.string.pubkey), Base64.DEFAULT);
            byte[] prv = Base64.decode(idk, Base64.URL_SAFE);
            output = cpabe.dec(pub, prv, encrypted_input);
            if (output != null) {
                if (key.equals("BatchID"))
                    value =  Integer.toString(new BigInteger(output).intValue());
                else
                    value = new String(output);
                jsonResult.put(key,value);

            } else {
                jsonResult.put(key,"Decryption failed! Your attributes do not satisfy the encryption policty");
            }
        }
        return jsonResult;

    }

    private String DownloadFileFromURLandDec(String filehash, String base_url) {

        String result;

        int DECRYPTING_FILE = 2;
        int INTEGRITY_CHECK_FAILED = -2;
        String ATTRIBUTE_FAILED = "{\"error\":\"You do not have the required attributes to decrypt this file.\"}";
        String FILE_NOT_FOUND = "{\"error\":\"File not found.\"}";
        String EXCEPTION = "{\"error\":\"Critical errors.\"}";
        byte[] dec_output;
        byte[] output_bytes;
        int count;
        try {
            URL url = new URL(base_url);
            URLConnection conection = url.openConnection();
            conection.connect();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            File tempfile = File.createTempFile("temp.cpabe", null, getCacheDir());
            String enc_file_path = tempfile.getAbsolutePath();
            OutputStream output = new FileOutputStream(tempfile);
            //ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;

                // writing data to byte array
                output.write(data, 0, count);
            }

            //output_bytes = output.toByteArray();
            output.flush();
            output.close();
            input.close();
            //Download completed
            Log.d("DDAsyncTask", "downloaded");

            Log.d("DDAsyncTask", "Checking file hash");
            //check hash value of downloaded file
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            String enc_file_hash = Hex.toHexString(md.digest(Common.suckFile(enc_file_path)));

            //ByteArrayInputStream bis = new ByteArrayInputStream(output_bytes);
            dec_output = decrypt(enc_file_path);
            result = new String(dec_output);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            result = FILE_NOT_FOUND;
            return result;

        } catch (AttributeException e) {
            result = ATTRIBUTE_FAILED;
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            result = EXCEPTION;
            return result;
        }

    }

}
