package com.finconsgroup.cosmeu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.finconsgroup.cosmeu.qrcode.QRCodeActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    JavaScriptInterface JSInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        WebView webView = findViewById(R.id.webView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            webView.getSettings().setSafeBrowsingEnabled(false);
        }

        webView.getSettings().setJavaScriptEnabled(true);
        JSInterface = new JavaScriptInterface(this);
        webView.addJavascriptInterface(JSInterface, "JSInterface");
        webView.loadUrl(getResources().getString(R.string.conf_webapp));



    }


    public class JavaScriptInterface {
        Context mContext;
        ProgressDialog progressDialog ;
        /** Instantiate the interface and set the context */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void CpabeKeyToAndroidStarted()
        {
            Log.e("***********CpabeKeyToAndroidStarted********************","*******************************");
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
        }

        @JavascriptInterface
        public void CpabeKeyToAndroidFinished(String key)
        {
            try {
                JSONObject json_response= new JSONObject(key);
                Log.e("***********CpabeKeyToAndroidFinished********************",key);
                if(json_response.getInt("status_code")==0){
                    if(progressDialog!=null) progressDialog.dismiss();
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    Intent intent =null;
                    editor.putString("prv_path",json_response.getString("private_key"));
                    editor.commit();
                    intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    if(progressDialog!=null) progressDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("ERROR");
                    builder.setMessage(json_response.getString("message"));
                    builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent =null;
                            intent = new Intent(LoginActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    }

}

