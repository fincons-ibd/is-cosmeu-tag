package com.finconsgroup.cosmeu.qrcode;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.SubtitleCollapsingToolbarLayout;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.finconsgroup.cosmeu.R;
import com.finconsgroup.cosmeu.ar.augmentedimage.AugmentedImageActivity;
import com.finconsgroup.cosmeu.history.HistoryActivity;
import com.github.florent37.glidepalette.GlidePalette;
import com.google.ar.core.ArCoreApk;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsLayoutInflater2;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


public class QRCodeRenderActivity extends AppCompatActivity {
    //TODO: add base_url to strings.xml
    private String base_url ;
    private Toolbar toolbar;
    private SubtitleCollapsingToolbarLayout collapsing_toolbar_layout;
    FloatingActionButton fab_ar ;
    private JSONObject full_json_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        base_url = getResources().getString(R.string.conf_base_url);
        try {
            full_json_info= new JSONObject(getIntent().getExtras().getString("full_json_info"));
            updateUI(full_json_info);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        fab_ar = findViewById(R.id.fab_ar);
        maybeEnableArButton();

    }

    void maybeEnableArButton() {
        ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(this);
        if (availability.isTransient()) {
            // Re-query at 5Hz while compatibility is checked in the background.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    maybeEnableArButton();
                }
            }, 200);
        }
        if (availability.isSupported()) {
            fab_ar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(QRCodeRenderActivity.this, AugmentedImageActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("full_json_info", full_json_info.toString());
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });
            // indicator on the button.
        } else { // Unsupported or unknown.
            fab_ar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Your smartphone isn't compatible with AR !!", Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void updateUI (JSONObject result){

        try {
            JSONObject json_file = result.getJSONObject("File");
            JSONObject json_production = result.getJSONObject("Production_info");
            JSONObject json_delivery = result.getJSONObject("Delivery_info");

            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            // TASTO BACK TOOLBAR
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            ImageView image_scrolling_top = findViewById(R.id.image_scrolling_top);
            collapsing_toolbar_layout= findViewById(R.id.collapsing_toolbar_layout);
            collapsing_toolbar_layout.setTitle(json_file.get("Product_name").toString());
            collapsing_toolbar_layout.setExpandedTitleTextAppearance(R.style.Expanded_Toolbar_Title);
            collapsing_toolbar_layout.setExpandedSubtitleTextAppearance(R.style.Expanded_Toolbar_SubTitle);
            collapsing_toolbar_layout.setSubtitle(json_file.get("Brand_name").toString());
            JSONObject json_file_multimedia = json_file.getJSONObject("Multimedia_assets");
            //Glide.with(this).load("http://www.nobile1942.it/easyUp/image/4_finestra_cover_sito_oe9u.jpg")
            //.listener(GlidePalette.with("http://www.nobile1942.it/easyUp/image/4_finestra_cover_sito_oe9u.jpg")
            Glide.with(this).load(base_url+json_file_multimedia.getString("Home_image"))
                    .listener(GlidePalette.with(base_url+json_file_multimedia.getString("Home_image"))
                            .intoCallBack(
                                    new GlidePalette.CallBack() {
                                        @Override
                                        public void onPaletteLoaded(Palette palette) {
                                            int muted = palette.getMutedColor(0);
                                            int vibrant = palette.getVibrantColor(0);
                                            int muted_dark = palette.getDarkMutedColor(0);
                                            int vibrant_dark = palette.getDarkVibrantColor(0);
                                            int muted_light = palette.getLightMutedColor(0);
                                            int vibrant_light = palette.getLightVibrantColor(0);

                                            if(vibrant !=0){
                                                fab_ar.setBackgroundTintList(ColorStateList.valueOf(vibrant_light));
                                                collapsing_toolbar_layout.setBackgroundColor(vibrant);
                                                collapsing_toolbar_layout.setStatusBarScrimColor(vibrant);
                                                collapsing_toolbar_layout.setContentScrimColor(vibrant);
                                                //getWindow().setNavigationBarColor(muted_dark);
                                                getWindow().setStatusBarColor(manipulateColor(vibrant,0.8f));
                                            }else if (muted !=0){
                                                fab_ar.setBackgroundTintList(ColorStateList.valueOf(muted_light));
                                                collapsing_toolbar_layout.setBackgroundColor(muted);
                                                collapsing_toolbar_layout.setStatusBarScrimColor(muted);
                                                collapsing_toolbar_layout.setContentScrimColor(muted);
                                                //getWindow().setNavigationBarColor(muted_dark);
                                                getWindow().setStatusBarColor(manipulateColor(muted,0.8f));
                                            }

                                            if(vibrant !=0 && vibrant_light ==0 ){
                                                fab_ar.setBackgroundTintList(ColorStateList.valueOf(Color.BLACK));
                                            }else if (muted !=0 && muted_light==0) {
                                                fab_ar.setBackgroundTintList(ColorStateList.valueOf(Color.BLACK));
                                            }
                                        }
                                    })
                    )
                    .into(image_scrolling_top);

            TextView authenticText = findViewById(R.id.textAuthentic);
            if(result.getBoolean("authentic")){
                authenticText.setText(R.string.authenticText);
                authenticText.setTextColor(Color.parseColor("#006600"));
            }else{
                authenticText.setText(R.string.notAuthenticText);
                authenticText.setTextColor(Color.parseColor("#850d1b"));
            }

            TableLayout tablelayout_features = findViewById(R.id.tablelayout_features);
            TableRow tablelayout_features_concentration = findViewById(R.id.tablelayout_features_concentration);
            TextView tablelayout_features_concentration_value = findViewById(R.id.tablelayout_features_concentration_value);
            if (json_file.has("Concentration")) {
                tablelayout_features_concentration_value.setText(json_file.getString("Concentration"));
            }else{
                tablelayout_features_concentration.setVisibility(View.GONE);
            }

            TableRow tablelayout_features_size = findViewById(R.id.tablelayout_features_size);
            TextView tablelayout_features_size_value = findViewById(R.id.tablelayout_features_size_value);
            if (json_file.has("Size")) {
                tablelayout_features_size_value.setText(json_file.getString("Size")+" ml");
            }else{
                tablelayout_features_size.setVisibility(View.GONE);
            }

            TableRow tablelayout_features_version = findViewById(R.id.tablelayout_features_version);
            TextView tablelayout_features_version_value = findViewById(R.id.tablelayout_features_version_value);
            if (json_file.has("Version")) {
                tablelayout_features_version_value.setText(json_file.getString("Version"));
            }else{
                tablelayout_features_version.setVisibility(View.GONE);
            }

            TableRow tablelayout_features_ean_code = findViewById(R.id.tablelayout_features_ean_code);
            TextView tablelayout_features_ean_code_value = findViewById(R.id.tablelayout_features_ean_code_value);
            if (json_file.has("Ean_code")) {
                tablelayout_features_ean_code_value.setText(json_file.getString("Ean_code"));
            }else{
                tablelayout_features_ean_code.setVisibility(View.GONE);
            }

            TableRow tablelayout_production_batch_id = findViewById(R.id.tablelayout_production_batch_id);
            TextView tablelayout_production_batch_id_value = findViewById(R.id.tablelayout_production_batch_id_value);
            if (json_production.has("BatchID")) {
                tablelayout_production_batch_id_value.setText(json_production.getString("BatchID"));
            }else{
                tablelayout_production_batch_id.setVisibility(View.GONE);
            }

            TableRow tablelayout_production_batch_date = findViewById(R.id.tablelayout_production_batch_date);
            TextView tablelayout_production_batch_date_value = findViewById(R.id.tablelayout_production_batch_date_value);
            if (json_production.has("BatchDate")) {
                tablelayout_production_batch_date_value.setText(json_production.getString("BatchDate"));
            }else{
                tablelayout_production_batch_date.setVisibility(View.GONE);
            }

            TableRow tablelayout_production_batch_market = findViewById(R.id.tablelayout_production_batch_market);
            TextView tablelayout_production_batch_market_value = findViewById(R.id.tablelayout_production_batch_market_value);
            if (json_delivery.has("BatchMarket") && !json_delivery.getString("BatchMarket").contains("failed")) {
                tablelayout_production_batch_market_value.setText(json_delivery.getString("BatchMarket"));
            }else{
                tablelayout_production_batch_market.setVisibility(View.GONE);
            }

            TableRow tablelayout_production_distributor = findViewById(R.id.tablelayout_production_distributor);
            TextView tablelayout_production_distributor_value = findViewById(R.id.tablelayout_production_distributor_value);
            if (json_delivery.has("BatchDistributor")&& !json_delivery.getString("BatchDistributor").contains("failed")) {
                tablelayout_production_distributor_value.setText(json_delivery.getString("BatchDistributor"));
            }else{
                tablelayout_production_distributor.setVisibility(View.GONE);
            }

            TextView textview_long_description = findViewById(R.id.textview_long_description);
            TextView textview_long_description_value = findViewById(R.id.textview_long_description_value);
            if (json_file.has("Long_description")&& !json_file.getString("Long_description").contains("failed")) {
                textview_long_description_value.setText(json_file.getString("Long_description"));
            }else{
                textview_long_description_value.setVisibility(View.GONE);
                textview_long_description.setVisibility(View.GONE);
            }

            JSONArray head_notes= json_file.getJSONArray("Head_notes");
            TextView textview_head_note = findViewById(R.id.textview_head_note);
            RecyclerView head_notes_recycler_view= findViewById(R.id.head_notes_recycler_view);
            if (json_file.has("Head_notes")) {
                HorizontalNotesAdapter Heart_notesAdapter=new HorizontalNotesAdapter(this, head_notes);
                head_notes_recycler_view.setAdapter(Heart_notesAdapter);
                Heart_notesAdapter.notifyDataSetChanged();
                LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                head_notes_recycler_view.setLayoutManager(horizontalLayoutManagaer);
                head_notes_recycler_view.setAdapter(Heart_notesAdapter);
            }else{
                head_notes_recycler_view.setVisibility(View.GONE);
                textview_head_note.setVisibility(View.GONE);
            }

            JSONArray heart_notes= json_file.getJSONArray("Heart_notes");
            TextView textview_heart_notes = findViewById(R.id.textview_head_note);
            RecyclerView heart_notes_recycler_view= findViewById(R.id.heart_notes_recycler_view);
            if (json_file.has("Heart_notes")) {
                HorizontalNotesAdapter Heart_notesAdapter=new HorizontalNotesAdapter(this, heart_notes);
                heart_notes_recycler_view.setAdapter(Heart_notesAdapter);
                Heart_notesAdapter.notifyDataSetChanged();
                LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                heart_notes_recycler_view.setLayoutManager(horizontalLayoutManagaer);
                heart_notes_recycler_view.setAdapter(Heart_notesAdapter);
            }else{
                heart_notes_recycler_view.setVisibility(View.GONE);
                textview_heart_notes.setVisibility(View.GONE);
            }

            JSONArray base_notes= json_file.getJSONArray("Base_notes");
            TextView textview_base_notes = findViewById(R.id.textview_base_note);
            RecyclerView base_notes_recycler_view= findViewById(R.id.base_notes_recycler_view);
            if (json_file.has("Heart_notes")) {
                HorizontalNotesAdapter base_notesAdapter=new HorizontalNotesAdapter(this, base_notes);
                base_notes_recycler_view.setAdapter(base_notesAdapter);
                base_notesAdapter.notifyDataSetChanged();
                LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                base_notes_recycler_view.setLayoutManager(horizontalLayoutManagaer);
                base_notes_recycler_view.setAdapter(base_notesAdapter);
            }else{
                base_notes_recycler_view.setVisibility(View.GONE);
                textview_base_notes.setVisibility(View.GONE);
            }


            SliderLayout sliderLayout = findViewById(R.id.imageSlider);
            sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
            sliderLayout.setScrollTimeInSec(5); //set scroll delay in seconds :

            JSONArray gallery_images = json_file_multimedia.getJSONArray("Gallery");
            for (int i = 0; i < gallery_images.length(); i++) {
                SliderView sliderView = new SliderView(this);
                sliderView.setImageUrl(base_url+gallery_images.getString(i));
                sliderLayout.setIndicatorAnimation(SliderLayout.Animations.WORM); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
                sliderLayout.addSliderView(sliderView);
            }



        } catch (JSONException e) {
            e.printStackTrace();
            finish();
        }


    }

    public static int manipulateColor(int color, float factor) {
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * factor);
        int g = Math.round(Color.green(color) * factor);
        int b = Math.round(Color.blue(color) * factor);
        return Color.argb(a,
                Math.min(r,255),
                Math.min(g,255),
                Math.min(b,255));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem history = menu.findItem(R.id.history);
        history.setIcon(new IconicsDrawable(this)
                .icon("gmd-restore")
                .color(Color.WHITE)
                .sizeDp(24));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        switch (item.getItemId()) {
            case R.id.history:
                //REMOVE ELEMENT FROM HISTORY
                try {
                    JSONArray saved_date = new JSONArray( pref.getString("full_json_info", "[]"));
                    if(saved_date.length()>0){
                        Intent intent = new Intent(QRCodeRenderActivity.this, HistoryActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(getApplicationContext(), "No history ! Please scan a QRcode", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }



}
